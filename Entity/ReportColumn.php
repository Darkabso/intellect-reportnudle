<?php

namespace Intellect\ReportBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Intellect\ReportBundle\Utils\Tools;

/**
 * ReportColumn
 *
 * @ORM\Table(name="intellect_report_column")
 * @ORM\Entity(repositoryClass="Intellect\ReportBundle\Repository\ReportColumnRepository")
 */
class ReportColumn
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="row", type="integer")
     */
    private $row;

    /**
     * @ORM\Column(name="value", type="string", length=500)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Intellect\ReportBundle\Entity\ReportColumnType", inversedBy="report_column")
     * @ORM\JoinColumn(name="report_column_type_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $report_column_type;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set row
     *
     * @param integer $row
     * @return ReportColumn
     */
    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return integer 
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ReportColumn
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        $value = null;

        switch ($this->report_column_type->getColumnType()) {
            case 'datetime':
                $value = (Tools::is_date($this->value)) ? new \DateTime($this->value) : null;
                break;
            case 'int':
                $value = (is_numeric($this->value)) ? (int)($this->value) : null;
                break;
            case 'float':
                $value = (is_float($this->value)) ? (float)($this->value) : null;
                break;
            case 'string':
                $value = $this->value;
                break;
        }

        if(empty($value)) { throw new \Exception('Column type is incorect.'); }

        return $value;
    }

    /**
     * Set report_column_type
     *
     * @param \Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType
     * @return ReportColumn
     */
    public function setReportColumnType(\Intellect\ReportBundle\Entity\ReportColumnType $reportColumnType = null)
    {
        $this->report_column_type = $reportColumnType;

        return $this;
    }

    /**
     * Get report_column_type
     *
     * @return \Intellect\ReportBundle\Entity\ReportColumnType 
     */
    public function getReportColumnType()
    {
        return $this->report_column_type;
    }
}

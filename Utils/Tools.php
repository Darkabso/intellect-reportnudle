<?php
namespace Intellect\ReportBundle\Utils;

class Tools
{
    static public function is_date($date)
    {
        try {
            $date = new \DateTime($date);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    static public function convertColumn($type, $value)
    {
        $result = null;

        switch ($type) {
            case 'datetime':
                $result = (Tools::is_date($value)) ? new \DateTime($value) : null;
                break;
            case 'int':
                $result = (is_numeric($value)) ? (int)($value) : null;
                break;
            case 'float':
                $result = (is_float($value)) ? (float)($value) : null;
                break;
            case 'string':
                $result = $value;
                break;
        }

        if(empty($result)) { throw new \Exception('Column type is incorect.'); }

        return $result;
    }
}
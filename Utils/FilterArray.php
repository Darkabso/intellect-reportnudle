<?php

namespace Intellect\ReportBundle\Utils;


class FilterArray
{
    private $array_filter, $column, $column_type, $filter_type, $first_row, $value;
    private $choice_filter = ['equal', 'moreThan', 'lessThan','moreThanOrEqual', 'lessThanOrEqual'];
    private $choice_filter_string = ['equal', 'diff'];

    public function __construct($array_filter, $column, $value, $filter_type)
    {
        $this->array_filter = $array_filter;
        $this->first_row = array_shift($array_filter);
        $this->column = $this->returnColumnIfExists($column);
        $this->column_type = $this->getColumnType();
        $this->filter_type = $this->checkFilterType($filter_type);
        $this->value = $this->returnVauleIfIsCompatibleWithColumnType($value);
    }

    private function returnColumnIfExists($column)
    {
        $columns = array_keys($this->first_row);
        if(in_array($column, $columns, true)){
            return $column;
        }
        throw new \Exception('Column not exists');
    }

    private function returnVauleIfIsCompatibleWithColumnType($value)
    {
        if( ($this->column_type === 'numericOrDatetime'
                && (is_numeric($value) || $this->value instanceof \DateTime))
            || ($this->column_type === 'string' && is_string($value))
        ) {
            return $value;
        }
        throw new \Exception('Incorrect value type');
    }

    private function checkFilterType($filter_type)
    {
        if( ($this->column_type === 'numericOrDatetime' && in_array($filter_type, $this->choice_filter, true))
            || ($this->column_type === 'string' && in_array($filter_type, $this->choice_filter_string, true))
        ) {
            return $filter_type;
        }
        return 'equal';
    }

    private function getColumnType()
    {
        $column_value = $this->first_row[$this->column];
        if( is_numeric($column_value)
            || $column_value instanceof \DateTime
        ) {
            return 'numericOrDatetime';
        }
        return 'string';
    }

    private function filterString($var)
    {
        if($this->filter_type === 'diff') {
            return $var[$this->column] !== $this->value;
        }
        return $var[$this->column] === $this->value;
    }

    private function filterNumberOrDatetime($var)
    {
        switch ($this->filter_type) {
            case 'equal':
                return $var[$this->column] === $this->value;
                break;
            case 'moreThan':
                return $var[$this->column] > $this->value;
                break;
            case 'lessThan':
                return $var[$this->column] < $this->value;
                break;
            case 'moreThanOrEqual':
                return $var[$this->column] >= $this->value;
                break;
            case 'lessThanOrEqual':
                return $var[$this->column] <= $this->value;
                break;
            default:
                return $var[$this->column] === $this->value;
                break;
        }
    }

    private function getFilterMethod()
    {
        if($this->column_type === 'numericOrDatetime') {
            return 'filterNumberOrDatetime';
        }
        return 'filterString';
    }

    public function getFilterArray()
    {
        $this->array_filter = array_filter($this->array_filter, array($this, $this->getFilterMethod()));
        return $this->array_filter;
    }
}
<?php

namespace Intellect\ReportBundle\Traits;

use Intellect\ReportBundle\Entity\ReportType;
use Intellect\ReportBundle\Utils\FilterArray;
use Intellect\ReportBundle\Utils\SortArray;
use Intellect\ReportBundle\Utils\Tools;

trait ReportRepository
{
    public function createReportType($type, $label, array $report_column_type)
    {
        $em = $this->getEntityManager();
        $repo_report_column_type = $em->getRepository('IntellectReportBundle:ReportColumnType');

        $report_type = new ReportType();
        $report_type->setType($type)
            ->setLabel($label);

        foreach ($report_column_type as $key => $value) {
            $report_column_type = $repo_report_column_type->createColumnType($report_type, $key, $value);
            $report_type->addReportColumnType($report_column_type);
        }

        $em->persist($report_type);
        $em->flush();

        return $report_type;
    }

    public function findReportTypeByType($type)
    {
        $repo_report_type = $this->getEntityManager()->getRepository('IntellectReportBundle:ReportType');
        return $repo_report_type->findOneBy(array('type' => $type));
    }

    public function createReport($report_type_name, array $report_array)
    {
        $em = $this->getEntityManager();
        $repo_report_column = $em->getRepository('IntellectReportBundle:ReportColumn');
        $report_type = $this->findReportTypeByType($report_type_name);

        $report = new $this->report_object();
        $report->setReportType($report_type);
        $repo_report_column->createReportColumns($report, $report_array);

        $em->persist($report);
        $em->flush();

        return $report;
    }

    public function getReport($report, array $sort = array(), array $filters = array())
    {
        $em = $this->getEntityManager();
        $repo_report_column = $em->getRepository('IntellectReportBundle:ReportColumn');
        $report_array = $repo_report_column->getColumnsForReport($report);
        $report_result = array();

        if (empty($report_array)) {
            return $report_array;
        }

        foreach ($report_array as $column) {
            $report_result[$column['row']][$column['col']] = Tools::convertColumn($column['type'], $column['value']);
        }

        if (!empty($filters) && !empty($report_result)) {
            $report_result = $this->multipleFilter($report_result, $filters);
        }

        if (!empty($sort) && !empty($report_result)) {
            $report_result = $this->multipleSort($report_result, $sort);
        }

        return $report_result;
    }

    private function multipleSort(array $array_sort, array $sort)
    {
        $sort = array_reverse($sort);
        foreach ($sort as $key => $value) {
            $sort = new SortArray($array_sort, $key, $value);
            $array_sort = $sort->getSortArray();
        }
        return $array_sort;
    }

    private function multipleFilter(array $array_sort, array $filters)
    {
        $filters = (array_key_exists('column', $filters)) ? array($filters) : $filters;
        foreach ($filters as $filter) {
            $filter = new FilterArray($array_sort, $filter['column'], $filter['value'], $filter['operator']);
            $array_sort = $filter->getFilterArray();
        }
        return $array_sort;
    }
}
